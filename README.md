# ISSSR Frontend Repository #

This is the frontend for the exam of "Ingegneria dei Sistemi Software e dei Servizi di Rete".<br>
The application's goal is to give to the user the possibility to do data analysis for a food restaurants and bars chain.<br>
This tool give the possibility to an expert user to create custom index, calculate its value along a period and save in a .pdf report.<br>
Eveything with the most easy UI.

# Getting Started ##
### Prerequisites ###

* Download the ISSSR Backend and run it

```
 git clone https://gitlab.com/Bebetos/isssr_backend.git
 ```
* Install Apache Web Server or similar

### Installing ###

* Copy the project into the Apache Web Server folder
* Enjoy it!

### Team ###

* Alberto Talone
* Andrea Cenciarelli
* Giorgio Marini
* Emanuele Serrao
* Fabio Di Giacomo
* Fabrizio Guglietti